local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local S = minetest.get_translator(modname)

minetest.register_craft({
	output = "mcl_chests:chest 4",
	recipe = {
		{"group:tree", "group:tree", "group:tree"},
		{"group:tree", "", "group:tree"},
		{"group:tree", "group:tree", "group:tree"},
	}
})

minetest.register_craft({
	output = "mcl_core:stick 16",
	recipe = {
		{"group:tree"},
		{"group:tree"},
	}
})

minetest.register_craft({
	type = "shapeless",
	output = "mcl_core:flint",
	recipe = { "mcl_core:gravel", "mcl_core:gravel", "mcl_core:gravel", },
})