This modpack is the official Modpack for MineClone2 and includes things not in the main game that will enhance the gameplay experience of Mineclone 2.

This modpack may make changes to your worlds, that you may not be able to revert back to a standard Mineclone 2 world. Noted possibilities would be unknown items and unknown nodes, if you remove the pack.

Creators:

Michieal

AncientMariner

PrairieWind (beginning ideas, and hopefully a few mods too)

License: 

Code: GPL3, unless otherwise stated by the author.

All other assets' license: CC-BY-SA, unless otherwise stated by the creator.