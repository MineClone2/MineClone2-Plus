--- Created by SmokeyDope
--- See License.txt and Additional Terms.txt for licensing.
--- If you did not receive a copy of the license with this content package, please see:
--- https://www.gnu.org/licenses/gpl-3.0.en.html and https://creativecommons.org/licenses/by-sa/4.0/

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

dofile(modpath.."/armor.lua")
dofile(modpath.."/tools.lua")
