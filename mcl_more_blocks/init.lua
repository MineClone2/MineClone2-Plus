local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local S = minetest.get_translator(modname)

minetest.register_node("mcl_more_blocks:brick_prism_block", {
	description = S("Brick Prism Block"),
	_doc_items_longdesc = S("Alternative brick variant for building."),
	tiles = {"brick_prism.png"},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=1, building_block=1, material_stone=1},
	sounds = mcl_sounds.node_sound_stone_defaults(),
	_mcl_blast_resistance = 6,
	_mcl_hardness = 2,
})
minetest.register_node("mcl_more_blocks:snow_brick_tiles", {
	description = S("Snow Brick Tiles"),
	_doc_items_longdesc = S("Snow brick tiles are cosmetic building blocks made from regular snow blocks."),
	_doc_items_hidden = false,
	tiles = {"mcl_more_blocks_snow_brick_tiles.png"},
	is_ground_content = true,
	stack_max = 64,
	groups = {shovely=2, building_block=1, snow_cover=1},
	sounds = mcl_sounds.node_sound_snow_defaults(),
	on_construct = mcl_core.on_snow_construct,
	after_destruct = mcl_core.after_snow_destruct,
	_mcl_blast_resistance = 0.1,
	_mcl_hardness = 2
})

minetest.register_node("mcl_more_blocks:snow_bricks", {
	description = S("Snow Bricks"),
	_doc_items_longdesc = S("Snow bricks are cosmetic building blocks made from regular snow blocks."),
	_doc_items_hidden = false,
	tiles = {"mcl_more_blocks_snow_bricks.png"},
	is_ground_content = true,
	stack_max = 64,
	groups = {shovely=2, building_block=1, snow_cover=1},
	sounds = mcl_sounds.node_sound_snow_defaults(),
	on_construct = mcl_core.on_snow_construct,
	after_destruct = mcl_core.after_snow_destruct,
	_mcl_blast_resistance = 0.1,
	_mcl_hardness = 2
})