---
--- Generated by EmmyLua
--- Created by Michieal.
--- DateTime: 02/07/2023 6:20 AM
--- Copyright (C) 2023, Michieal. See License.txt
--- Last Update Time: 02/07/23 6:55 PM
--- License: See Readme.txt in this module.
--- If you did not receive a copy of the license with this content package, please see:
--- Readme.txt in this module.

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local S = minetest.get_translator(modname)

-- register normal user access to debug levels 1 and 0.
minetest.register_chatcommand("modpackver", {
	description = S("Display Mineclone 2 Plus Modpack version."),
	func = function(name, _) -- removed params variable as it was unused.
		local conf = Settings(modpath .. "/ver.conf")
		local version = conf:get("version")
		local game_info ={}
		game_info.title = "Mineclone 2 Plus"

		if version == nil or version == "" then
			version = "0.1.0 running on Minetest 5.6.1 or unsupported."
		end
		game_info.version = version

		minetest.chat_send_player(name, string.format("Version: %s - %s", game_info.title, game_info.version))
		return true
	end
})

