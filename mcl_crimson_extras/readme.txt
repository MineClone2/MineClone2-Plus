Created by Michieal.

Some textures in this pack are from Mineclone 2's mcl_crimson module. In most cases, they have been heavily modified by
myself, mostly in coloration. This is intentional, as part of what this mod does is change the purple "warped forest"
blocks / items to their green variants. See below for source images creators.

Textures: CC-BY-SA 4.0 (international)
Code: The code licence is GPL3.

https://www.gnu.org/licenses/gpl-3.0.en.html and https://creativecommons.org/licenses/by-sa/4.0/

One final request -- the Date-Time Creator code block needs to remain in the code files. This allows everyone to know
which version of the code is being used, and allows for better troubleshooting of any errors.

Init.Lua's original Date-Time Creator code block:
---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by Michieal.
--- DateTime: 01/19/23 4:50 PM
--- Copyright (C) 2023, Michieal. See License.txt
--- Last Update Time: [Last Update Time]
--- License: See Readme.txt in this module.
--- See main License.txt and Additional Terms.txt for licensing.
--- If you did not receive a copy of the license with this content package, please see:
--- Readme.txt in this module.
---

Source Images Creators:
Mcl_crimson_extras_door_warped_side (upper & lower): JoMW1998.
Mcl_crimson_extras_warped_hyphae_side: Michieal (the original was created by Micheal and included in mcl2).
All other textures:
Originals: See Mineclone 2's CREDITS.md, section Textures for credits.
Derivatives: Michieal.
